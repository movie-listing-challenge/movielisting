import { Header } from "./components/Header";
import { MovieList } from "./components/MovieList";
import { useEffect, useState } from "react";
import { api } from "./common/api";
import { LoadingContainer } from "./components/Loading";
import { HEADER_HEIGHT } from "./common/constants";
import styled from "styled-components";

function App() {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    api.initialize().then(() => {
      setIsLoading(false);
    });
  }, []);

  return (
    <StyledApp>
      <Header />

      <StyledContent>
        <LoadingContainer loading={isLoading}>
          <MovieList />
        </LoadingContainer>
      </StyledContent>
    </StyledApp>
  );
}

const StyledApp = styled.div`
  height: 100vh;
  width: 100vw;
`;

const StyledContent = styled.div`
  width: 100%;
  height: calc(100% - ${HEADER_HEIGHT}px);

  padding-top: ${HEADER_HEIGHT}px;
`;

export default App;

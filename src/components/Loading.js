import styled from "styled-components";

import { CREAM, GREEN, RED, YELLOW } from "../common/color";

export const LoadingContainer = ({ loading, children, small }) => {
  return loading ? (
    <StyledLoading>
      <StyledLoadingAnimation $small={small} />
    </StyledLoading>
  ) : (
    children
  );
};

const StyledLoading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: 100%;
`;

const StyledLoadingAnimation = styled.div`
  border: 8px solid;
  border-top-color: ${RED};
  border-right-color: ${GREEN};
  border-bottom-color: ${YELLOW};
  border-left-color: ${CREAM};

  border-radius: 50%;
  width: ${({ $small }) => ($small ? "30px" : "60px")};
  height: ${({ $small }) => ($small ? "30px" : "60px")};
  animation: spin 2s linear infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;

import { useCallback, useEffect, useState } from "react";
import styled from "styled-components";
import debounce from "lodash.debounce";

import { api } from "../common/api";
import {
  CONTENT_HORIZONTAL_PADDING,
  CONTENT_VERTICAL_PADDING,
  MAX_CONTENT_WIDTH,
} from "../common/constants";
import { Filters } from "./Filters";
import { MovieCard } from "./MovieCard";
import { LoadingContainer } from "./Loading";
import { RED } from "../common/color";

export const MovieList = () => {
  const [movies, setMovies] = useState(undefined);
  const [filteredMovies, setFilteredMovies] = useState();
  const [availableGenres, setAvailableGenres] = useState();
  const [filters, setFilters] = useState({ rating: 3 });

  const debouncedSetFilters = useCallback(
    debounce((rating, genres) => {
      setFilters({
        rating,
        genres,
      });
    }, 750),
    []
  );

  useEffect(() => {
    api.getMoviesNowPlaying().then((response) => {
      // Sort by popularity and map genres to make filtering faster
      setMovies(
        response.results
          .sort((movie1, movie2) => movie2.popularity - movie1.popularity)
          .map((movie) => {
            movie.genres = {};
            movie.genre_ids.forEach((genreId) => {
              movie.genres[genreId] = true;
            });
            return movie;
          })
      );
    });
  }, []);

  useEffect(() => {
    if (movies) {
      // Find available genres for filtering
      const genreIdsMap = movies.reduce((genres, movie) => {
        movie.genre_ids.forEach((genreId) => {
          genres[genreId] = true;
        });

        return genres;
      }, {});

      setAvailableGenres(Object.keys(genreIdsMap));
    }
  }, [movies]);

  // Filter movies based on rating and genres
  useEffect(() => {
    if (movies && filters) {
      setFilteredMovies(
        movies.filter((movie) => {
          const genresList = filters.genres ? Object.keys(filters.genres) : [];
          return (
            movie.vote_average > filters.rating &&
            (genresList.length < 1 ||
              genresList.reduce((acc, genre) => {
                return acc && !!movie.genres[genre];
              }, true))
          );
        })
      );
    }
  }, [filters, movies]);

  return (
    <Page>
      <StyledContent>
        <Filters
          availableGenres={availableGenres}
          onFilter={(rating, genres) => {
            debouncedSetFilters(rating, genres);
          }}
        />
        <LoadingContainer loading={movies === undefined}>
          {filteredMovies && filteredMovies.length > 0 ? (
            <StyledMovieGrid>
              {filteredMovies.map((movie) => (
                <MovieCard key={movie.id} movie={movie} />
              ))}
            </StyledMovieGrid>
          ) : (
            <StyledMessage>
              Sorry, it appears there are no movies matching your filters
            </StyledMessage>
          )}
        </LoadingContainer>
      </StyledContent>
    </Page>
  );
};

const Page = styled.div`
  display: flex;
  justify-content: center;
`;

const StyledContent = styled.div`
  padding: ${CONTENT_VERTICAL_PADDING}px ${CONTENT_HORIZONTAL_PADDING}px
    ${CONTENT_VERTICAL_PADDING}px;

  max-width: ${MAX_CONTENT_WIDTH}px;
  width: 100%;

  display: grid;
  grid-template-columns: min-content auto;
  grid-gap: 40px;

  @media (max-width: 600px) {
    grid-template-columns: 1fr;
  }
`;

const StyledMovieGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 20px;

  @media (max-width: 980px) {
    grid-template-columns: repeat(3, 1fr);
  }

  @media (max-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
  }

  @media (max-width: 600px) {
    grid-template-columns: 1fr;
  }
`;

const StyledMessage = styled.div`
  border: 1px solid ${RED};
  color: ${RED};
  font-size: 14px;
  font-weight: 500;
  text-align: center;

  width: 100%;
  height: min-content;
  padding: 10px;

  display: flex;
  justify-content: center;
  align-items: center;
`;

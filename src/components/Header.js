import styled from "styled-components";

import { CREAM, GRAY } from "../common/color";
import {
  CONTENT_HORIZONTAL_PADDING,
  HEADER_HEIGHT,
  MAX_CONTENT_WIDTH,
} from "../common/constants";

export const Header = () => {
  return (
    <StyledHeader>
      <StyledHeaderContent>
        <StyledTitle>Movie Listings</StyledTitle>
      </StyledHeaderContent>
    </StyledHeader>
  );
};

const StyledHeader = styled.header`
  display: flex;
  justify-content: center;
  height: ${HEADER_HEIGHT}px;
  width: 100%;

  position: fixed;

  background-color: ${GRAY};
`;

const StyledHeaderContent = styled.div`
  max-width: ${MAX_CONTENT_WIDTH}px;
  width: 100%;
  padding: 0 ${CONTENT_HORIZONTAL_PADDING}px;

  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const StyledTitle = styled.div`
  font-size: 24px;
  font-weight: 700;

  color: ${CREAM};
`;

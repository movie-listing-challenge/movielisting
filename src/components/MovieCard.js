import styled from "styled-components";

import { api } from "../common/api";
import { GRAY, GREEN } from "../common/color";

export const MovieCard = ({ movie }) => {
  return (
    <StyledCard>
      <StyledPoster src={api.getImageLink(movie.poster_path)} />
      <StyledCardContent>
        <StyledTitle>{movie.original_title}</StyledTitle>
        <StyledGenres>
          {movie.genre_ids.map((genreID) => (
            <StyledGenre key={genreID}>{api.movieGenres[genreID]}</StyledGenre>
          ))}
        </StyledGenres>
      </StyledCardContent>
    </StyledCard>
  );
};

const StyledCard = styled.div`
  display: flex;
  flex-direction: column;

  box-shadow: 0px 0px 6px 0px #00000023;
  transition: box-shadow 0.3s;

  &:hover {
    box-shadow: 2px 3px 6px 0px #00000029;
  }
`;

const StyledCardContent = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  padding: 10px;
`;

const StyledPoster = styled.img`
  width: 100%;
`;

const StyledTitle = styled.div`
  font-size: 16px;
  font-weight: 500;
  color: ${GRAY};

  margin-bottom: 10px;
  flex-grow: 1;
`;

const StyledGenres = styled.div`
  display: flex;
  flex-wrap: wrap-reverse;
  width: 100;
  justify-content: flex-end;
  align-items: flex-end;
  align-content: flex-end;
`;

const StyledGenre = styled.div`
  border: 1px solid ${GREEN};
  border-radius: 3px;

  color: ${GREEN};
  padding: 2px 4px;

  font-size: 10px;
  margin: 2px;
`;

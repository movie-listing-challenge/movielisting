import { useEffect, useState } from "react";
import styled from "styled-components";

import { api } from "../common/api";
import { GRAY, GREEN, RED, YELLOW } from "../common/color";
import { LoadingContainer } from "./Loading";

export const Filters = ({ availableGenres, onFilter }) => {
  const [rating, setRating] = useState(3);
  const [genres, setGenres] = useState({});

  useEffect(() => {
    onFilter(rating, genres);
  }, [rating, genres, onFilter]);

  return (
    <div>
      <StyledFilters>
        <LoadingContainer loading={!availableGenres} small>
          <StyledTitle>Filters:</StyledTitle>
          <SliderSection>
            <StyledRatings>
              <span>0</span>
              <StyledRatingsTitle>Rating</StyledRatingsTitle>
              <span>10</span>
            </StyledRatings>
            <StyledSlider
              type="range"
              min="0"
              max="10"
              id="range"
              step="0.5"
              defaultValue="3"
              onChange={(event) => {
                setRating(event.target.value);
              }}
            />
            <StyledRatingValue>{rating}</StyledRatingValue>
          </SliderSection>
          <GenresSection>
            <GenresTitle>Genres</GenresTitle>
            <GenresContainer>
              {availableGenres &&
                availableGenres.map((genre) => (
                  <StyledCheckboxLabel key={genre}>
                    <StyledCheckboxInput
                      type="checkbox"
                      onClick={(event) => {
                        const updatedGenres = { ...genres };
                        if (event.target.checked) {
                          updatedGenres[genre] = true;
                        } else {
                          delete updatedGenres[genre];
                        }

                        setGenres(updatedGenres);
                      }}
                    />
                    <StyledCheckbox>{api.movieGenres[genre]}</StyledCheckbox>
                  </StyledCheckboxLabel>
                ))}
            </GenresContainer>
          </GenresSection>
        </LoadingContainer>
      </StyledFilters>
    </div>
  );
};

const StyledFilters = styled.div`
  border: 1px solid ${GRAY};
  border-radius: 3px;

  padding: 10px;
  width: 200px;
  height: auto;

  @media (max-width: 768px) {
    width: 150px;
  }

  @media (max-width: 600px) {
    width: 100%;
  }
`;

const StyledTitle = styled.div`
  font-size: 16px;
  padding-bottom: 10px;
  border-bottom: 1px solid ${GRAY};
  color: ${GRAY};
`;

const SliderSection = styled.div`
  padding: 10px;

  border-bottom: 1px solid #00000020;
`;

const StyledSlider = styled.input`
  -webkit-appearance: none;
  width: 100%;
  height: 10px;
  background: ${YELLOW};
  opacity: 1;
  -webkit-transition: 0.2s;
  transition: opacity 0.2s;
  border-radius: 10px;

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 15px;
    height: 15px;
    background: ${RED};
    cursor: pointer;
    border-radius: 10px;
    opacity: 0.8;
  }

  &::-moz-range-thumb {
    width: 15px;
    height: 15px;
    background: ${RED};
    cursor: pointer;
    border-radius: 10px;
    opacity: 0.8;
  }

  &::-moz-range-thumb,
  &::-webkit-slider-thumb {
    opacity: 1;
  }
`;

const StyledRatings = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;

  width: 100%;
  padding-bottom: 3px;

  font-size: 12px;
  color: ${GRAY};
`;

const StyledRatingsTitle = styled.span`
  font-size: 14px;
  font-weight: 500;
  color: ${GRAY};
`;

const StyledRatingValue = styled.div`
  width: 100%;
  text-align: center;

  font-size: 14px;
  font-weight: 700;
  color: ${GRAY};
`;

const GenresSection = styled.div`
  margin-top: 10px;
`;

const GenresTitle = styled.span`
  font-size: 14px;
  font-weight: 500;
  color: ${GRAY};
`;

const GenresContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 10px;
`;

const StyledCheckbox = styled.div`
  border: 1px solid #00000040;
  border-radius: 3px;

  color: #00000080;
  padding: 2px 4px;

  font-size: 10px;
  margin: 2px;

  @media (max-width: 600px) {
    font-size: 14px;
    margin: 4px;
  }
`;

const StyledCheckboxInput = styled.input`
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
`;

const StyledCheckboxLabel = styled.label`
  display: block;
  position: relative;
  cursor: pointer;
  user-select: none;

  ${StyledCheckboxInput}:checked ~ ${StyledCheckbox} {
    color: ${GREEN};
    border: 1px solid ${GREEN};
  }
`;

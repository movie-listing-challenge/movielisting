export const GRAY = "#3D405B";
export const CREAM = "#F4F1DE";
export const RED = "#E07A5F";
export const GREEN = "#81B29A";
export const YELLOW = "#F2CC8F";
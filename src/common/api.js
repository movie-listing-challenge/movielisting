const API_KEY = "be4c64ea9ee30e0d39f23f53feb9ee56";

class API {
  imagesBaseUrl;
  imageSizes;
  movieGenres = [];

  initialize() {
    return new Promise(async (resolve, reject) => {
      const configuration = await this.getConfiguration();
      const genres = await this.getGenres();

      if (configuration && genres) {
        this.imagesBaseUrl = configuration.images.base_url;
        this.imageSizes = configuration.images.poster_sizes;

        // Re-map genres to hashmap for direct access
        genres.genres.forEach((genre) => {
          this.movieGenres[genre.id] = genre.name;
        });

        resolve();
      } else {
        reject();
      }
    });
  }

  fetchApi = (url) => {
    return fetch(`
          https://api.themoviedb.org/3/${url}?api_key=${API_KEY}
          `).then((response) => response.json());
  };

  getConfiguration = () => {
    return fetchApi("configuration");
  };

  getGenres = () => {
    return fetchApi("/genre/movie/list");
  };

  getMoviesNowPlaying = () => {
    return fetchApi("movie/now_playing");
  };

  getImageLink = (imagePath) => {
    return this.imagesBaseUrl + this.imageSizes[4] + imagePath;
  };
}

export const api = new API();

const fetchApi = (url) => {
  return fetch(`
    https://api.themoviedb.org/3/${url}?api_key=${API_KEY}
    `).then((response) => response.json());
};
